<?php

/**
 * Log Survey Participant update on survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2025 Denis Chenu <www.sondages.pro>
 * @copyright 2020-2021 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL v3
 * @version 1.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class LogSurveyParticipant extends PluginBase
{
    protected static $name = 'LogSurveyParticipant';
    protected static $description = 'Allow to log update on survey by participant';

    protected $storage = 'DbStorage';

    /* @var array the settings, needed for sending email via CRON */
    protected $settings = array(
        'hostInfo' => array(
            'type' => 'string',
            'label' => 'Host info for url',
        ),
        'baseUrl' => array(
            'type' => 'string',
            'label' => 'baseUrl',
        )
    );

    /* @var log level on echo */
    private $echoLevel = \CLogger::LEVEL_TRACE; //\CLogger::LEVEL_INFO;

    /* @var integer surveyId */
    private $surveyId;
    /* @var integer responseId */
    private $responseId;
    /* @var array response attributes */
    private $aInitialResponse;

    private static function getDontCompareResponse()
    {
        return array(
            'id',
            'seed',
            'token',
            'submitdate',
            'lastpage',
            'startlanguage',
            'startdate',
            'datestamp'
        );
    }

    public function init()
    {
        Yii::setPathOfAlias('LogSurveyParticipant', dirname(__FILE__));
        /* Create table */
        $this->subscribe('beforeActivate');

        /* The settings */
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');

        /* The action */
        $this->subscribe('beforeSurveyPage');

        /* In case of other action (by plugin for example */
        $this->subscribe('beforeResponseSave', 'beforeResponseSave');
        $this->subscribe('beforeSurveyDynamicSave', 'beforeResponseSave');

        /* deletion of response : happen only when deactivate survey */
        $this->subscribe('afterSurveyDelete');
        $this->subscribe('afterSurveySave');
        $this->subscribe('afterResponseDelete');
        $this->subscribe('afterSurveyDynamicDelete', 'afterResponseDelete');

        /** Add the list inside survey **/
        $this->subscribe('newQuestionAttributes');
        $this->subscribe('beforeQuestionRender');

        /* Cron action */
        $this->subscribe('direct', 'consoleAction');
        /* Test purpose */
        //$this->subscribe('newDirectRequest','testAction');

        /** Alow edit twig **/
        $this->subscribe('getValidScreenFiles');
    }

    /**
    * Add table before activate
    */
    public function beforeActivate()
    {
        $oEvent = $this->getEvent();
        if (!defined('reloadAnyResponse\Utilities::API')) {
            $oEvent->set('success', false);
            $oEvent->set('message', 'You need reloadAnyResponse with API 4.2 minimum');
            return;
        }
        if (reloadAnyResponse\Utilities::API < 4.2) {
            $oEvent->set('success', false);
            $oEvent->set('message', 'You need reloadAnyResponse with API 4.2 minimum');
            return;
        }
        $this->insertUpdateTable();
        if (is_null($this->get('hostInfo'))) {
            if (Yii::app() instanceof CConsoleApplication) {
                $event->set('success', false);
                $event->set('message', 'This plugin need to be configurated before activate.');
                return;
            }
            $settings = array(
                'hostInfo' => Yii::app()->request->getHostInfo(),
                'baseUrl' => Yii::app()->request->getBaseUrl(),
                'scriptUrl' => Yii::app()->request->getScriptUrl(),
            );
            $this->saveSettings($settings);
            $oEvent->set('message', 'Default configuration for url is used.');
        }
    }

    /**
     * Set the needed table
     * No need DB version currently, juts create when actuvate
     */
    private function insertUpdateTable()
    {

        $sCollation = '';
        if (in_array(App()->db->driverName, array('mysql', 'mysqli', 'mssql'))) {
            $sCollation = "COLLATE 'utf8mb4_bin'";
        }
        if (in_array(App()->db->driverName, array('sqlsrv', 'dblib', 'mssql'))) {
            $sCollation = "COLLATE SQL_Latin1_General_CP1_CS_AS";
        }
        /* Flush cache before update */
        if (method_exists(Yii::app()->cache, 'flush')) {
            Yii::app()->cache->flush();
        }
        if (method_exists(Yii::app()->cache, 'gc')) {
            Yii::app()->cache->gc();
        }
        if (!$this->api->tableExists($this, 'response')) {
            $this->api->createTable($this, 'response', array(
                'id' => 'pk',
                'sid' => 'int NOT NULL',
                'srid' => 'int NOT NULL',
                'datestamp' => 'datetime NOT NULL',
                'updatecount' => 'int',
                'state' => 'longbinary' // as json
            ));
            $tableName = $this->api->getTable($this, 'response')->tableName();
            App()->getDb()->createCommand()->createIndex('sid_idx', $tableName, 'sid');
            App()->getDb()->createCommand()->createIndex('sid_srid_idx', $tableName, array('sid','srid'));
            App()->setFlashMessage("response table created");
        } else {
            App()->setFlashMessage("response table already exist", 'warning');
        }
        if (!$this->api->tableExists($this, 'participant')) {
            $this->api->createTable($this, 'participant', array(
                'id' => 'pk',
                'sid' => 'int NOT NULL',
                'srid' => 'int NOT NULL',
                'token' => "string(35) {$sCollation}",
                'userdescription' => "text",
                'useremail' => "text",
                'datestamp' => 'datetime', // lastaction
                'count' => 'int', // total count of update
            ));
            $tableName = $this->api->getTable($this, 'participant')->tableName();
            App()->getDb()->createCommand()->createIndex('sid_idx', $tableName, 'sid');
            App()->getDb()->createCommand()->createIndex('sid_srid_idx', $tableName, array('sid','srid'));
            App()->setFlashMessage("participant table created");
        } else {
            App()->setFlashMessage("participant table already exist", 'warning');
        }
    }

    /**
     * The survey settings
     * @return void
     */
    public function beforeSurveySettings()
    {
        $event = $this->event;
        $surveyId = $event->get('survey');
        $oSurvey = Survey::model()->findByPk($surveyId);
        /* Check help */
        $help = null;
        $issues = array();
        if ($oSurvey->isAnonymized) {
            $issues[] = $this->gT("survey is anonymous");
        }
        if (!$oSurvey->hasTokensTable) {
            $issues[] = $this->gT("survey didn't have token table");
        }
        if (!$oSurvey->datestamp == 'Y') {
            $issues[] = $this->gT("survey is not date stamped");
        }
        if (!empty($issues)) {
            $help = CHTml::tag(
                "div",
                array('class' => 'text-danger'),
                CHTml::tag("strong", array(), $this->gT("Unable to log survey participant:")) .
                CHTml::tag(
                    "ul",
                    array('class' => ''),
                    "<li>" . implode("</li><li>", $issues) . "</li>"
                )
            );
        }
        $aTokenAttributeList = array();
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            $aTokenAttributeList[$attribute] = empty($information['description']) ? $attribute : $information['description'];
        }
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        $lastLog = $this->getValidDateTime($this->get('lastLog', 'Survey', $surveyId, $oSurvey->datecreated));
        if (empty($lastLog)) {
            $lastLog = $oSurvey->datecreated;
        }
        $event->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'LogSurveyParticipant' => array(
                    'type' => 'boolean',
                    'label' => $this->gT('Log participant action by token.'),
                    'current' => $this->get('LogSurveyParticipant', 'Survey', $surveyId, 0),
                    'help' => $help,
                ),
                'sendLogTo' => array(
                    'type' => 'string',
                    'label' => $this->gT('List of email to send all action log (with cron or direct event)'),
                    'current' => $this->get('sendLogTo', 'Survey', $surveyId),
                    'help' => $this->gT('Using detailed admin response template from related initiative survey'),
                ),
                'sendTokenAdminLog' => array(
                    'type' => 'boolean',
                    'label' => $this->gT('Send action log by group to group manager.'),
                    'current' => $this->get('sendTokenAdminLog', 'Survey', $surveyId, 0),
                    'help' => $this->gT('Using basic admin response template from related initiative survey'),
                ),
                'useFirstNameLastName' => array(
                    'type' => 'boolean',
                    'label' => $this->gT('Use first name and last name for token description.'),
                    'current' => $this->get('useFirstNameLastName', 'Survey', $surveyId, 1),
                ),
                'useAttributesDescription' => array(
                    'type' => 'select',
                    'options' => $aTokenAttributeList,
                    'htmlOptions' => array(
                        'multiple' => true,
                        'placeholder' => gT("None"),
                        'unselectValue' => "",
                    ),
                    'selectOptions' => array(
                        'placeholder' => gT("None"),
                    ),
                    'label' => $this->gT('Attributes used for token description.'),
                    'current' => $this->get('useAttributesDescription', 'Survey', $surveyId),
                ),
                'columnForTitle' => array(
                    'type' => 'select',
                    'options' => $aQuestionList['data'],
                    'htmlOptions' => array(
                        'empty' => gT("None"),
                    ),
                    'current' => $this->get('columnForTitle', 'Survey', $surveyId),
                    'label' => $this->gT('Column used for title in email.') ///$this->gT('Column used for title in email and delete action.'),
                ),
                'lastLog' => array(
                    'type' => 'string',
                    'current' => $lastLog,
                    'label' => $this->gT('Date start.'),
                    'help' => $this->gT('Automatically set, no need to update. You can force the serach of response logger start. Use YYYY-MM-DD [HH[:ii[:ss]]] format.'),
                ),
                'disableGroup' => array(
                    'type' => 'string',
                    'label' => $this->gT('Group disable'),
                    'help' => $this->gT('Disable sending email to specific group manager. Separated by ,.'),
                    'current' => $this->get('disableGroup', 'Survey', $surveyId, ''),
                ),
            ),
        ));
    }

    /**
     * Save survey settings
     * @return void
     */
    public function newSurveySettings()
    {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            if ($name == "lastLog") {
                $value = self::getValidDateTime($value);
            }
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    /**
     * Add the attributes
     */
    public function newQuestionAttributes()
    {
        if (!defined('reloadAnyResponse\Utilities::API') || reloadAnyResponse\Utilities::API < 4.2) {
            return;
        }
        $listAttributes = array(
            'NumberShowLastParticpant' => array(
                'name'      => 'NumberShowLastParticpant',
                'types'     => 'X', /*Only text display */
                'category'  => $this->translate('User management'),
                'sortorder' => 910,
                'inputtype' => 'integer',
                'min' => '0',
                'caption' => $this->translate('Number of participant to show : LogSurveyParticipant'),
                'help' => $this->translate("If the action of registering participants by token is enabled, you can display the list of the last users with a token. This setting is part of the LogSurveyParticipant plugin."),
                'default'   => 0,
            ),
            'AgeShowLastParticpant' => array(
                'name'      => 'AgeShowLastParticpant',
                'types'     => 'X', /*Only text display */
                'category'  => $this->translate('User management'),
                'sortorder' => 920,
                'inputtype' => 'integer',
                'min' => '1',
                'caption'   => $this->translate('Maximum age'),
                'help' => $this->translate('Maximum age in days after the participant accesses the response. Before this age : particpant are not shown in this list.'),
                'default'   => 30,
            ),
            'ShowCurrentShowLastParticpant' => array(
                'name'      => 'ShowCurrentShowLastParticpant',
                'types'     => 'X', /*Only text display */
                'category'  => $this->translate('User management'),
                'sortorder' => 930,
                'inputtype' => 'switch',
                'options'   => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption'   => $this->translate('Show the current participant in the list'),
                'default'   => 0,
            ),
        );
        $this->getEvent()->append('questionAttributes', $listAttributes);
    }

    /**
     * Add the list in question text
     */
    public function beforeQuestionRender()
    {
        if (!defined('reloadAnyResponse\Utilities::API') || reloadAnyResponse\Utilities::API < 4.2) {
            return;
        }
        if ($this->getEvent()->get('type') != "X") {
            return;
        }
        if (!$this->isActiveFor($this->getEvent()->get('surveyId'))) {
            return;
        }
        $surveyId = $this->getEvent()->get('surveyId');
        if (!reloadAnyResponse\Utilities::getCurrentSrid($surveyId)) {
            return;
        }
        $srid = reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        $token = reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($this->getEvent()->get('qid'));
        if (empty($aAttributes['NumberShowLastParticpant'])) {
            return;
        }
        $NumberShowLastParticpant = intval($aAttributes['NumberShowLastParticpant']);
        if (!$NumberShowLastParticpant) {
            return;
        }
        $beforeQuestionRenderEvent = $this->getEvent();
        $criteria = new CDbCriteria();
        $criteria->select = array(
            'sid',
            'srid',
            'token',
            'userdescription',
            'sum(count) as count',
            'max(datestamp) as datestamp'
        );
        $criteria->compare("sid", $surveyId);
        $criteria->compare("srid", $srid);
        if (empty($aAttributes['ShowCurrentShowLastParticpant'])) {
            $criteria->compare("token", "<>" . $token);
        }
        $criteria->group = "sid, srid, token, userdescription";
        $AgeShowLastParticpant = intval($aAttributes['AgeShowLastParticpant']);
        if (!$AgeShowLastParticpant) {
            $AgeShowLastParticpant = 30;
        }
        $since = dateShift("{$AgeShowLastParticpant} days ago", "Y-m-d H:i:s", App()->getConfig('timeadjust'));
        $criteria->compare("datestamp", ">=" . $since);
        $criteria->order = "datestamp DESC";
        $criteria->limit = $NumberShowLastParticpant;
        $ParticipantsLog = \LogSurveyParticipant\models\Participant::model()->findAll($criteria);
        $Participants = array();
        foreach ($ParticipantsLog as $ParticipantLog) {
            $Participants[$ParticipantLog->token] = $ParticipantLog->attributes;
            if (defined('TokenUsersListAndManagePlugin\Utilities::API')) {
                Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
                $oToken = TokenManaged::model($surveyId)->findByToken($ParticipantLog->token);
            } else {
                $oToken = Token::model($surveyId)->findByToken($ParticipantLog->token);
            }
            if ($oToken) {
                $Participants[$ParticipantLog->token]['tokenattributes'] = $oToken->getAttributes();
                if (isset($oToken->active)) {
                    $Participants[$ParticipantLog->token]['active'] = $oToken->active;
                }
            }
        }
        $renderDataTwig = array(
            'Participants' => $Participants,
            'language' => array(
                'Previous participants' => $this->translate("Previous participants"),
                'Previous participants since %s days' => $this->translate("Previous participants since %s days"),
                'Previous participants since yesterday' => $this->translate("Previous participants since yesterday"),
                'Previous participants since %s' => $this->translate("Previous participants since %s"),
                'No previous participants' => $this->translate("No previous participants"),
                'No previous participants since %s days' => $this->translate("No previous participants since %s days"),
                'No revious participants since yesterday' => $this->translate("No previous participants since yesterday"),
                'No previous participants since %s' => $this->translate("No previous participants since %s"),
                'Last action' => $this->translate("Last action"),
                'Is not active' => $this->translate("Is not active"),

            ),
            'HaveTokenUsersListAndManagePlugin' => defined('TokenUsersListAndManagePlugin\Utilities::API'),
            'NumberShowLastParticpant' => $NumberShowLastParticpant,
            'AgeShowLastParticpant' => $AgeShowLastParticpant,
            'SinceShowLastParticpant' => $since,
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage())
        );
        $this->subscribe('getPluginTwigPath');
        $renderFile = "subviews/survey/question_subviews/logsurveyparticipant_participantlist.twig";
        $extraPart = App()->twigRenderer->renderPartial(
            $renderFile,
            $renderDataTwig
        );
        $this->unsubscribe('getPluginTwigPath');

        $question = $beforeQuestionRenderEvent->get('text');
        $question .= $extraPart;
        $beforeQuestionRenderEvent->set('text', $question);
    }

    /**
     * Check if neeeded, get current attrivute
     */
    public function beforeSurveyPage()
    {
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $this->unsubscribe('beforeResponseSave');
        $this->unsubscribe('beforeSurveyDynamicSave');
        if (!$this->isActiveFor($surveyId)) {
            return;
        }
        if (!App()->getRequest()->getIsPostRequest()) {
            return;
        }
        $this->surveyId = $surveyId;
        $srid = null;
        if (!empty($_SESSION['survey_' . $surveyId]['srid'])) {
            $srid = $this->responseId = $_SESSION['survey_' . $surveyId]['srid'];
            $oResponse = Response::model($this->surveyId)->findByPk($srid);
            if ($oResponse) {
                $this->aInitialResponse = $oResponse->getAttributes();
            }
        }
        $this->subscribe('afterSurveyComplete', 'logParticipantAfterSurveyComplete');
        $this->subscribe('getPluginTwigPath', 'logParticipantPluginTwigPath');
    }

    /**
     * Log update reponse after complete (or quota)
     * @return void
     */
    public function logParticipantAfterSurveyComplete()
    {
        $this->unsubscribe('getPluginTwigPath');
        $this->unsubscribe('afterResponseSave');
        $this->unsubscribe('afterSurveyDynamicSave');
        if (!$this->surveyId) {
            return;
        }
        $responseId = $this->getEvent()->get('responseId');
        $oResponse = Response::model($this->getEvent()->get('surveyId'))->findByPk($responseId);
        if (!$oResponse) {
            return;
        }
        $this->logParticipantReponseUpdate($oResponse);
    }

    /**
     * Log update reponse when page display
     * @return void
     */
    public function logParticipantPluginTwigPath()
    {
        $this->unsubscribe('getPluginTwigPath');
        $this->unsubscribe('afterSurveyComplete');
        $this->unsubscribe('afterResponseSave');
        $this->unsubscribe('afterSurveyDynamicSave');
        if (!$this->surveyId) {
            return;
        }
        if (!$this->responseId) {
            if (!empty($_SESSION['survey_' . $this->surveyId]['srid'])) {
                $this->responseId = $_SESSION['survey_' . $this->surveyId]['srid'];
            }
        }
        if (!$this->responseId) {
            return;
        }

        $oResponse = Response::model($this->surveyId)->findByPk($this->responseId);
        if (!$oResponse) {
            return;
        }
        $this->logParticipantReponseUpdate($oResponse);
    }

    /**
     * Log update reponse via model save events
     * @return void
     */
    public function logParticipantAfterReponseSave()
    {
        $this->unsubscribe('getPluginTwigPath');
        $this->unsubscribe('afterResponseSave');
        $this->unsubscribe('afterSurveyDynamicSave');
        if (!$this->surveyId) {
            return;
        }
        $oResponse = $this->getEvent()->get('model');
        if (!$oResponse) {
            return;
        }
        $this->logParticipantReponseUpdate($oResponse);
    }

    /**
     * Log the participant
     * @param \Reponse  after saved
     * @return void
     */
    public function logParticipantReponseUpdate($oResponse)
    {
        $this->unsubscribe('getPluginTwigPath');
        $this->unsubscribe('afterResponseSave');
        $this->unsubscribe('afterSurveyDynamicSave');
        if (!$this->surveyId) {
            return;
        }
        if (!$this->responseId) {
            /* No response id set : check if have a new one */
            $this->responseId = $oResponse->id;
        }
        if ($this->responseId != $oResponse->id) {
            // Throw 500 error ?
        }
        if (empty($oResponse->token)) {
            return;
        }
        $aInitialResponse = $this->aInitialResponse;
        $aCurrentResponse = $oResponse->getAttributes();

        $userEmail = "";
        $oToken = Token::model($this->surveyId)->find("token = :token", array(":token" => $oResponse->token));
        if ($oToken) {
            if ($this->get('useFirstNameLastName', 'Survey', $this->surveyId, 1)) {
                $userDescription = implode(" ", array_filter(array($oToken->firstname,$oToken->lastname)));
            }
            $userEmail = $oToken->email;
        }
        $userDescription = implode(" ", array($oToken->firstname,$oToken->lastname));
        $useAttributesDescription = $this->get('useAttributesDescription', 'Survey', $this->surveyId);
        if (!empty($useAttributesDescription) && is_array($useAttributesDescription)) {
            $aDescription = array();
            foreach ($useAttributesDescription as $attribute) {
                if ($oToken->getAttribute($attribute)) {
                    $aDescription[] = $oToken->getAttribute($attribute);
                }
            }
            if (!empty($aDescription)) {
                $userDescription = $userDescription . " (" . implode(" - ", $aDescription) . ")";
            }
        }
        \LogSurveyParticipant\models\Participant::addUpdate(array(
            'sid' => $this->surveyId,
            'srid' => $this->responseId,
            'token' => $oResponse->token,
            'userdescription' => $userDescription,
            'useremail' => $userEmail,
        ));
    }

    public function beforeResponseSave()
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        if (App()->controller->getId() == 'admin') {
            return;
        }

        $surveyId = $this->getEvent()->get('surveyId');
        if (!$this->isActiveFor($surveyId)) {
            $this->unsubscribe('beforeResponseSave');
            $this->unsubscribe('beforeSurveyDynamicSave');
            return;
        }
        $oResponse = $this->getEvent()->get('model');
        $srid = $oResponse->id;

        $oResponse = $this->getEvent()->get('model');
        $this->surveyId = $surveyId;
        if (!empty($oResponse->id)) {
            /* else 1st save */
            $this->responseId = $oResponse->id;
        }
        $this->aInitialResponse = $oResponse->getAttributes();
        $this->subscribe('afterResponseSave', 'logParticipantAfterReponseSave');
        $this->subscribe('afterSurveyDynamicSave', 'logParticipantAfterReponseSave');
    }

    /**
    * @see parent:getPluginSettings
    */
    public function getPluginSettings($getValues = true)
    {
        if (!Yii::app() instanceof CConsoleApplication) {
            $this->settings['hostInfo']['default'] = Yii::app()->request->getHostInfo();
            $this->settings['baseUrl']['default'] = Yii::app()->request->getBaseUrl();
        }
        return parent::getPluginSettings($getValues);
    }

    /** Deletion part */
    /** Survey delete : delete related */
    public function afterSurveyDelete()
    {
        $oSurvey = $this->getEvent()->get('model');
        \LogSurveyParticipant\models\Response::model()->deleteAllByAttributes(array('sid' => $oSurvey->sid));
        \LogSurveyParticipant\models\Participant::model()->deleteAllByAttributes(array('sid' => $oSurvey->sid));
    }
    /** Survey inactivate : delete related */
    public function afterSurveySave()
    {
        $oSurvey = $this->getEvent()->get('model');
        if (!$oSurvey->getIsActive()) {
            \LogSurveyParticipant\models\Response::model()->deleteAllByAttributes(array('sid' => $oSurvey->sid));
            \LogSurveyParticipant\models\Participant::model()->deleteAllByAttributes(array('sid' => $oSurvey->sid));
        }
    }
    /** Response delete : delete related */
    public function afterResponseDelete()
    {
        $oResponse = $this->getEvent()->get('model');
        $surveyId = $this->getEvent()->get('surveyId');
        \LogSurveyParticipant\models\Response::model()->deleteAllByAttributes(array('sid' => $surveyId, 'srid' => $oResponse->id));
        \LogSurveyParticipant\models\Participant::model()->deleteAllByAttributes(array('sid' => $surveyId, 'srid' => $oResponse->id));
        if (App()->getController() && App()->getController()->getId() == 'admin') {
            return;
        }
        /** Todo : log delete by user */
    }

    /**
     * The console action
     * php ./application/commands/console.php plugin direct --target=LogSurveyParticipant
     */
    public function consoleAction()
    {
        if ($this->event->get("target") != get_class()) {
            return;
        }
        if (!Yii::app() instanceof CConsoleApplication) {
            throw new CHttpException(403);
        }
        $this->fixLsCommand();
        $this->doLogForSurveys();
    }

    /**
     * Mimic console action if debug set
     */
    public function testAction()
    {
        if ($this->event->get("target") != get_class()) {
            return;
        }
        //~ if (!App()->getConfig('debug')) {
            //~ return;
        //~ }
        $this->SendAllMailLog();
        Yii::app()->end();
    }

    /**
     * Find all pugin with email
     * @return void
     */
    public function doLogForSurveys()
    {
        /* Get list of possible sureys */
        $aoSurveys = Survey::model()->active()->open()->findAll();
        $quoteKey = App()->getDb()->quoteColumnName("key");
        $aSurveyAdminMails = CHtml::listData(
            PluginSetting::model()->findAll(
                "plugin_id = :plugin_id and $quoteKey = :setting",
                array(":plugin_id" => $this->id,":setting" => 'sendLogTo')
            ),
            "model_id",
            "value"
        );
        $aSurveyGroupMails = CHtml::listData(
            PluginSetting::model()->findAll(
                "plugin_id = :plugin_id and $quoteKey = :setting",
                array(":plugin_id" => $this->id,":setting" => 'sendTokenAdminLog')
            ),
            "model_id",
            "value"
        );
        $aSurveyAdminMails = array_filter(array_map('json_decode', $aSurveyAdminMails));
        $aSurveyGroupMails = array_filter(array_map('json_decode', $aSurveyGroupMails));
        $aAllSurveys = array_keys($aSurveyAdminMails + $aSurveyGroupMails);
        foreach ($aAllSurveys as $surveyId) {
            if ($this->isActiveFor($surveyId)) {
                $this->doLogForSurvey($surveyId);
            }
        }
    }

    /**
     * send mail log to a specific survey
     * @param integer : surveyId
     * @param string : date from
     * @param string : date to
     * @return void
     */
    private function doLogForSurvey($surveyId)
    {
        $columnForTitle = $this->getFinalColumn($surveyId, $this->get('columnForTitle', 'Survey', $surveyId));
        if (empty($columnForTitle)) {
            $this->log("No columnForTitle for survey $surveyId", \CLogger::LEVEL_WARNING);
            return;
        }
        $dateLastCron = $this->getValidDateTime($this->get("lastLog", "Survey", $surveyId));
        if (empty($dateLastCron)) {
            $dateLastCron = Survey::model()->findByPk($surveyId)->datecreated;
        }
        $dateNow = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
        $this->log("Start survey $surveyId from $dateLastCron to $dateNow", \CLogger::LEVEL_INFO);
        /* Get the response list */
        $criteria = new CDBCriteria();
        $criteria->compare('datestamp', ">=" . $dateLastCron);
        $criteria->compare('datestamp', "<" . $dateNow);
        $criteria->order = 'datestamp DESC';
        $countResponses = Response::model($surveyId)->count($criteria);
        if (empty($countResponses)) {
            $this->log("No response for survey $surveyId", \CLogger::LEVEL_INFO);
            $this->setCurrentState($surveyId, $dateNow);
            return;
        }
        $this->log("Have response for survey $surveyId", \CLogger::LEVEL_INFO);
        if ($this->get('sendLogTo', 'Survey', $surveyId, "")) {
            $this->createAndSendWholeLog($surveyId, $dateLastCron, $dateNow);
        }
        if ($this->get('sendTokenAdminLog', 'Survey', $surveyId, 0)) {
            $this->createAndSendGroupLog($surveyId, $dateLastCron, $dateNow);
        }
        $this->setCurrentState($surveyId, $dateNow);
    }

    /**
     * Set the current state of each surveys
     * @param integer surveyid
     * @param string $to : date to
     * @return void
     */
    private function setCurrentState($surveyId, $to)
    {
        $criteria = new CDBCriteria();
        $criteria->compare('datestamp', "<" . $to);
        $criteria->order = 'datestamp DESC';
        $aoResponses = Response::model($surveyId)->findAll($criteria);
        foreach ($aoResponses as $oResponse) {
            $responseLog = LogSurveyParticipant\models\Response::model()->find(
                "sid = :sid and srid = :srid",
                array(
                    'sid' => $surveyId,
                    'srid' => $oResponse->id
                )
            );
            //if(empty($responseLog)) {
            if (true) {
                $responseLog = new LogSurveyParticipant\models\Response();
                $responseLog->sid = $surveyId;
                $responseLog->srid = $oResponse->id;
            }
            $responseLog->datestamp = $oResponse->datestamp;
            $dontCompareAttribute = $this->getDontCompareResponse();
            /* Create the current state */
            $currentState = array_diff_key(
                $oResponse->getAttributes(),
                array_combine($dontCompareAttribute, $dontCompareAttribute)
            );
            $currentState = array_map(
                array($this, 'replaceNewLine'),
                $currentState
            );
            $responseLog->state = json_encode($currentState);
            if (!$responseLog->save()) {
                $this->log("An error happen when try to save responseLog for {$surveyId} and {$oResponse->id}", \CLogger::LEVEL_WARNING);
            }
        }
        $this->set('lastLog', $to, 'Survey', $surveyId);
        $this->log("last log set to {$to} for {$surveyId}", \CLogger::LEVEL_INFO);
    }

    /**
     * send mail log to admin
     * @param integer : surveyId
     * @param \Response[] : valid response
     * @param string $from : date from
     * @param string $to : date to
     * @return void
     */
    private function createAndSendWholeLog($surveyId, $from, $to)
    {
        $adminEmails = trim($this->get("sendLogTo", "Survey", $surveyId));
        if (empty($adminEmails)) {
            return;
        }
        if ($adminEmails) {
            $aAdminEmails = preg_split("/(,|;)/", $adminEmails);
            $aAdminEmails = array_filter($aAdminEmails, function ($email) {
                return filter_var($email, FILTER_VALIDATE_EMAIL);
            });
            if (empty($aAdminEmails)) {
                return;
            }
        }
        $columnForTitle = $this->getFinalColumn($surveyId, $this->get('columnForTitle', 'Survey', $surveyId));
        if (empty($columnForTitle)) {
            return;
        }
        $criteria = new CDBCriteria();
        $criteria->compare('datestamp', ">=" . $from);
        $criteria->compare('datestamp', "<" . $to);
        $criteria->order = 'datestamp DESC';
        $aoResponses = Response::model($surveyId)->findAll($criteria);
        $aDataResponsesLogs = $this->getResponsesDataForLogs($surveyId, $aoResponses, $from, $to);
        $aSummaryLogs = $aDataResponsesLogs['summary'];
        $aResponsesLogs = $aDataResponsesLogs['aResponsesLogs'];
        if (empty($aResponsesLogs)) {
            return;
        }
        $aRenderData = array(
            'aSurveyInfo' => getSurveyInfo($surveyId),
            'aResponsesLogs' => $aResponsesLogs,
            'aSummaryLogs' => $aSummaryLogs,
            'columnForTitle' => $columnForTitle
        );
        $this->subscribe('getPluginTwigPath');
        Template::model()->getInstance(null, $surveyId);
        $rendered = App()->twigRenderer->renderPartial('/subviews/content/LogSurveyParticipant_mailAdmin.twig', $aRenderData);
        if (trim($rendered)) {
            /* Construct and send the email */
            $this->sendEmail($surveyId, 'admin_responses', $aAdminEmails, $rendered, $from);
        } else {
            $this->log("No content for email_admin_responses", \CLogger::LEVEL_INFO);
        }
    }

    /**
     * send mail log to group manager
     * @param integer : surveyId
     * @param \Response[] : valid response
     * @param string $from : date from
     * @return void
     */
    private function createAndSendGroupLog($surveyId, $from, $to)
    {
        /* Find if we have a group and a group manager */
        if (!Yii::getPathOfAlias('responseListAndManage')) {
            return;
        }
        $groupAttribute = responseListAndManage\Utilities::getSetting($surveyId, 'tokenAttributeGroup');
        $managerAttribute = responseListAndManage\Utilities::getSetting($surveyId, 'tokenAttributeGroupManager');
        if (empty($groupAttribute) || empty($managerAttribute)) {
            return;
        }
        if (!Survey::model()->findByPk($surveyId)->getHasTokensTable()) {
            return;
        }
        $aValidAttributes = Token::model($surveyId)->getAttributes();
        if (!array_key_exists($groupAttribute, $aValidAttributes) || !array_key_exists($managerAttribute, $aValidAttributes)) {
            $this->log("Invalid attributes for group or manager for survey $surveyId", \CLogger::LEVEL_WARNING);
            return;
        }
        $columnForTitle = $this->getFinalColumn($surveyId, $this->get('columnForTitle', 'Survey', $surveyId));
        if (empty($columnForTitle)) {
            $this->log("Invalid title column for $surveyId", \CLogger::LEVEL_WARNING);
            return;
        }
        $criteria = new CDBCriteria();
        $criteria->condition = "email <> '' AND TRIM($managerAttribute) <> '' AND TRIM($managerAttribute) <> '0' AND emailstatus = 'OK'";
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
        $criteria->addCondition("COALESCE(validuntil, '$now') >= '$now' AND COALESCE(validfrom, '$now') <= '$now'");
        $criteria->order = "$groupAttribute ASC";
        $oManagerTokens = Token::model($surveyId)->findAll($criteria);
        $currentGroup = "";
        $htmlContent = "";
        $aGroupTokens = array();
        $disableGroup = trim(strval($this->get('disableGroup', 'Survey', $surveyId, '')));
        $disableGroups = array_filter(explode(",", $disableGroup));
        foreach ($oManagerTokens as $oToken) {
            if (in_array($oToken->$groupAttribute, $disableGroups)) {
                $this->log("email disable for " . $oToken->$groupAttribute, \CLogger::LEVEL_INFO);
                continue;
            }
            if ($currentGroup != $oToken->$groupAttribute) {
                $aGroupTokens = responseListAndManage\Utilities::getTokensList($surveyId, $oToken->token);
                if ($aGroupTokens) {
                    $criteria = new CDBCriteria();
                    $criteria->compare('datestamp', ">=" . $from);
                    $criteria->compare('datestamp', "<" . $to);
                    $criteria->addInCondition('token', $aGroupTokens);
                    $criteria->order = 'datestamp DESC';
                    $aoResponses = Response::model($surveyId)->findAll($criteria);
                } else {
                    $aoResponses = array();
                }
                $this->log(count($aoResponses) . " responses updated for " . $oToken->$groupAttribute, \CLogger::LEVEL_TRACE);
            }
            if (!empty($aoResponses)) {
                $aDataResponsesLogs = $this->getResponsesDataForLogs($surveyId, $aoResponses, $from, $to, $oToken->token);
                $aSummaryLogs = $aDataResponsesLogs['summary'];
                $aResponsesLogs = $aDataResponsesLogs['aResponsesLogs'];
                if (empty($aResponsesLogs)) {
                    continue;
                }
                $aRenderData = array(
                    'aSurveyInfo' => getSurveyInfo($surveyId),
                    'aResponsesLogs' => $aResponsesLogs,
                    'aSummaryLogs' => $aSummaryLogs,
                    'columnForTitle' => $columnForTitle,
                    'oToken' => $oToken
                );
                $this->subscribe('getPluginTwigPath');
                Template::model()->getInstance(null, $surveyId);
                $rendered = App()->twigRenderer->renderPartial('/subviews/content/LogSurveyParticipant_mailManager.twig', $aRenderData);
                if (trim($rendered)) {
                    $aGroupEmails = preg_split("/(,|;)/", $oToken->email);
                    $aGroupEmails = array_filter($aGroupEmails, function ($email) {
                        return filter_var($email, FILTER_VALIDATE_EMAIL);
                    });
                    LimeExpressionManager::singleton()->loadTokenInformation($surveyId, $oToken->token);
                    /* Construct and send the email */
                    $this->sendEmail($surveyId, 'admin_notification', $aGroupEmails, $rendered, $from);
                } else {
                    $this->log(
                        sprintf(
                            "No content for %s : total : %s, new : %s, diff : %s, participant : %s.",
                            $oToken->email,
                            $aRenderData['aSummaryLogs']['total'],
                            $aRenderData['aSummaryLogs']['new'],
                            $aRenderData['aSummaryLogs']['withdiff'],
                            $aRenderData['aSummaryLogs']['withparticipant'],
                        ),
                        \CLogger::LEVEL_INFO
                    );
                }
            }
            $currentGroup = $oToken->$groupAttribute;
        }
    }

    /**
     * Return the data from array of response for twig contruction
     * @param integer : surveyId
     * @param \Response[] : valid response
     * @param string $from : date from
     * @param string $to : date to
     * @param string : $token to use for link
     * @return array[]
     */
    private function getResponsesDataForLogs($surveyId, $aoResponses, $from, $to, $token = null)
    {
        $columnForTitle = $this->getFinalColumn($surveyId, $this->get('columnForTitle', 'Survey', $surveyId));
        $dontCompareAttribute = self::getDontCompareResponse();
        $aoUserParticipants = array();
        $aResponsesLogs = array();
        $summary = array(
            'total' => 0,
            'new' => 0,
            'withdiff' => 0,
            'withparticipant' => 0,
        );
        foreach ($aoResponses as $oResponse) {
            if (trim($oResponse->getAttribute($columnForTitle) == "")) {
                /* Must delete ? */
                break;
            }
            $summary['total']++;
            /* @var temporary array[] to fill data more easily */
            $aAttributes = $oResponse->getAttributes();
            $aQuestionCodeList = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
            $aAnswers = array();
            foreach ($aQuestionCodeList as $column => $code) {
                $aAnswers[$code] = $aAttributes[$column];
            }
            /* @var temporary array[] to fill data more easily */
            $aResponsesLog = array(
                'attributes' => $aAttributes,
                'answers' => $aAnswers,
            );
            $aResponsesLog['new'] = true;
            $aResponsesLog['difference'] = null;
            /* Find the token updater */
            $criteria = new CDbCriteria();
            $criteria->select = 'sid, srid, token, userdescription, useremail, MAX(datestamp) as maxdate';
            $criteria->compare('sid', $surveyId);
            $criteria->compare('srid', $oResponse->id);
            $criteria->compare('datestamp', ">=" . $from);
            $criteria->compare('datestamp', "<" . $to);
            $criteria->group = 'sid, srid, userdescription, useremail, token';
            $criteria->order = 'maxdate DESC';
            $aoUserParticipants = \LogSurveyParticipant\models\Participant::model()->findAll($criteria);
            $aUserParticipants = array();
            foreach ($aoUserParticipants as $oUserParticipant) {
                $aUserParticipants[$oUserParticipant->id] = $oUserParticipant->getAttributes();
                if ($oUserParticipant->token && Survey::model()->findByPk($surveyId)->getHasTokensTable()) {
                    $oToken = Token::model($surveyId)->findByAttributes(array('token' => $oUserParticipant->token));
                    if ($oToken) {
                        $aUserParticipants[$oUserParticipant->id]['participant'] = $oToken->getAttributes();
                    }
                }
                $summary['withparticipant']++;
            }
            $aResponsesLog['aUserParticipants'] = $aUserParticipants;
            /* Create the current state */
            $currentState = array_diff_key(
                $aAttributes,
                array_combine($dontCompareAttribute, $dontCompareAttribute)
            );
            $currentState = array_map(
                array($this, 'replaceNewLine'),
                $currentState
            );
            /* Find the diffs */
            $criteria = new CDbCriteria();
            $criteria->compare('sid', $surveyId);
            $criteria->compare('srid', $oResponse->id);
            //~ $criteria->compare('datestamp',">=".$from);
            $criteria->compare('datestamp', "<" . $to);
            $criteria->order = 'datestamp DESC';
            $aoResponseLog[$oResponse->id] = \LogSurveyParticipant\models\Response::model()->find($criteria);
            if ($aoResponseLog[$oResponse->id]) {
                $aResponsesLog['new'] = false;
                $oResponseLog = $aoResponseLog[$oResponse->id];
                $previousState = json_decode($oResponseLog->state, 1);
                $dontCompareAttribute = self::getDontCompareResponse();
                $responseDifference = array_diff_assoc($previousState, $currentState);
                $aResponsesLog['difference'] = $responseDifference;
                if (count($responseDifference)) {
                    $summary['withdiff']++;
                }
            } else {
                $summary['new']++;
            }
            $aResponsesLog['completed'] = $oResponse->submitdate;
            $aResponsesLog['title'] = $oResponse->getAttribute($columnForTitle);
            $urlParams = array(
                'sid' => $surveyId,
                'srid' => $oResponse->id,
                'newtest' => "Y"
            );
            if ($token) {
                $urlParams['token'] = $token;
            } elseif (!empty($oResponse->token)) {
                $urlParams['token'] = $oResponse->token;
            }
            $aResponsesLog['publicurl'] = App()->createAbsoluteUrl(
                "survey/index",
                $urlParams
            );
            if ($token && defined('reloadAnyResponse\Utilities::API') && reloadAnyResponse\Utilities::API >= 4.2) {
                $startUrl = new reloadAnyResponse\StartUrl($surveyId, $token);
                $aResponsesLog['publicurl'] = $startUrl->getUrl(
                    $oResponse->id,
                    array('newtest' => 'Y'),
                    false,
                    true,
                    false // Disable checking admin right since sent by email
                );
            }
            $aResponsesLog['adminurl'] = Yii::app()->createAbsoluteUrl(
                "admin/dataentry/sa/editdata/subaction/edit",
                array(
                    'surveyid' => $surveyId,
                    'id' => $oResponse->id,
                )
            );
            $aResponsesLogs[$oResponse->id] = $aResponsesLog;
        }
        return array(
            'summary' => $summary,
            'aResponsesLogs' => $aResponsesLogs
        );
    }

    /**
     * Send the email to a lists of emails
     * @param integer $surveyId
     * @param string $type
     * @param string[] valid email address
     * @param string html to send
     * @param string from date when send
     * @return boolean
     */
    private function sendEmail($surveyId, $type, $aEmails, $bodyHtml, $dateFrom)
    {
        if (intval(Yii::app()->getConfig('versionnumber') < 4)) {
            return $this->sendEmailApi3($surveyId, 'email_' . $type, $aEmails, $bodyHtml, $dateFrom);
        }
        $mailer = \LimeMailer::getInstance(\LimeMailer::ResetComplete);
        $mailer->setSurvey($surveyId);
        $mailer->emailType = $type;
        $mailer->replaceTokenAttributes = true;
        $mailer->addUrlsPlaceholders(['SURVEY', 'RELOAD']);
        $mailer->setTypeWithRaw($type);
        /* Replace specific word for rawsubject and rawbody */
        $specificReplace = array(
            '{SURVEYDESCRIPTION}' => '{MESSAGE}',
        );
        $mailer->rawSubject = str_replace(
            array_keys($specificReplace),
            $specificReplace,
            $mailer->rawSubject
        );
        $mailer->rawBody = str_replace(
            array_keys($specificReplace),
            $specificReplace,
            $mailer->rawBody
        );
        if (Yii::getPathOfAlias('responseListAndManage')) {
            $mailer->aReplacements["RELOADURL"] = $mailer->aReplacements["SURVEYURL"] = App()->createAbsoluteUrl("plugins/direct", array('plugin' => 'responseListAndManage','sid' => $surveyId));
        }
        $mailer->aReplacements["MESSAGE"] = $bodyHtml;
        foreach ($aEmails as $email) {
            $mailer->setTo($email);
            if ($mailer->sendMessage()) {
                $this->log("email sent to $email", \CLogger::LEVEL_INFO);
            } else {
                $this->log("An error happen when send email to $email", \CLogger::LEVEL_WARNING);
            }
        }
    }

    /**
     * Send the email to a lists of emails
     * @param integer $surveyId
     * @param string $type
     * @param string[] valid email address
     * @param string html to send
     * @param string from date when send
     * @return boolean
     */
    private function sendEmailApi3($surveyId, $type, $aEmails, $bodyHtml, $dateFrom)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $from = $oSurvey->admin . " <" . $oSurvey->adminemail . ">";
        $aSurveyInfo = getSurveyInfo($surveyId, Survey::model()->findByPk($surveyId)->language);
        $baseSubject = $aSurveyInfo[$type . '_subj'];
        $baseMessage = $aSurveyInfo[$type];
        $aReplacementFields = array();
        $aReplacementFields["ADMINNAME"] = $aSurveyInfo['admin'];
        $aReplacementFields["ADMINEMAIL"] = empty($aSurveyInfo['adminemail']) ? App()->getConfig('siteadminemail') : $aSurveyInfo['adminemail'];
        $aReplacementFields["SURVEYNAME"] = $aSurveyInfo['name'];
        $aReplacementFields["SURVEYDESCRIPTION"] = $bodyHtml;
        if (Yii::getPathOfAlias('responseListAndManage')) {
            $aReplacementFields["RELOADURL"] = $aReplacementFields["SURVEYURL"] = App()->createAbsoluteUrl("plugins/direct", array('plugin' => 'responseListAndManage','sid' => $surveyId));
            foreach (array('RELOAD', 'SURVEY') as $key) {
                $url = $aReplacementFields["{$key}URL"];
                $aReplacementFields["{$key}URL"] = "<a href='{$url}'>" . htmlspecialchars($url) . '</a>';
            }
        }
        $aReplacementFields["EXPIRY"] = $dateFrom;
        $subject = \LimeExpressionManager::ProcessStepString($baseSubject, $aReplacementFields, 3, 1);
        $message = \LimeExpressionManager::ProcessStepString($baseMessage, $aReplacementFields, 3, 1);
        foreach ($aEmails as $email) {
            if (SendEmailMessage($message, $subject, $email, $from, App()->getConfig("sitename"), true, App()->getConfig("siteadminbounce"))) {
                $this->log("email sent to $email", \CLogger::LEVEL_INFO);
            } else {
                $this->log("An error happen when send email to $email", \CLogger::LEVEL_WARNING);
            }
        }
    }

    /** add the needed twig for emailing */
    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * For edition of twig
     */
    public function getValidScreenFiles()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (
            $this->getEvent()->get("type") != 'view' ||
            ($this->getEvent()->get("screen") && $this->getEvent()->get("screen") != "token")
        ) {
            return;
        }
        $this->subscribe('getPluginTwigPath');
        $this->getEvent()->append(
            'add',
            array(
                "subviews/content/LogSurveyParticipant_mailAdmin.twig",
                "subviews/content/LogSurveyParticipant_mailManager.twig"
            )
        );
    }

    /**
     * @inheritdoc, but echo according to echo level
     */
    public function log($message, $level = \CLogger::LEVEL_TRACE)
    {
        parent::log($message, $level);
        if (!Yii::app() instanceof CConsoleApplication) {
            return;
        }
        switch ($this->echoLevel) {
            case \CLogger::LEVEL_ERROR:
                if ($level == \CLogger::LEVEL_WARNING) {
                    return;
                }
            case \CLogger::LEVEL_WARNING:
                if ($level == \CLogger::LEVEL_INFO) {
                    return;
                }
            case \CLogger::LEVEL_INFO:
            default:
                if ($level == \CLogger::LEVEL_TRACE) {
                    return;
                }
            case \CLogger::LEVEL_TRACE:
                if ($level == \CLogger::LEVEL_PROFILE) {
                    return;
                }
            case \CLogger::LEVEL_PROFILE:
                break;
        }
        $sNow = date(DATE_ATOM);
        echo "[{$sNow}] [{$level}] $message\n";
    }

    /**
     * Check if log participent is active and can be activated
     * @param $surveyid
     * @return boolean
     */
    private function isActiveFor($surveyId)
    {
        if (!$this->get('LogSurveyParticipant', 'Survey', $surveyId)) {
            return false;
        }
        if (!Survey::model()->findByPk($surveyId)->isActive) {
            return false;
        }
        if (Survey::model()->findByPk($surveyId)->isAnonymized) {
            return false;
        }
        if (!Survey::model()->findByPk($surveyId)->hasTokensTable) {
            return false;
        }
        if (!Survey::model()->findByPk($surveyId)->isDateStamp) {
            return false;
        }
        return true;
    }
    /**
    * Replace new line to specific new line
    * Used to compare difference without new line difference in text.
    * @param string $string to fix
    * @param string $newline replaced by
    * @return string
    */
    private static function replaceNewLine($string, $newline = "\n")
    {
        if (version_compare(substr(PCRE_VERSION, 0, strpos(PCRE_VERSION, ' ')), '7.0') > -1) {
            return rtrim(preg_replace(array('~\R~u'), array($newline), $string));
        }
        return rtrim(str_replace(array("\r\n","\n", "\r"), array($newline,$newline,$newline), $string));
    }

    /**
     * Fix LimeSurvey command function
     * @todo : find way to control API
     * OR if another plugin already fix it
     */
    private function fixLsCommand()
    {
        if (!class_exists('DbStorage', false)) {
            include_once(dirname(__FILE__) . "/DbStorage.php");
        }
        if (!class_exists('ClassFactory', false)) {
            Yii::import('application.helpers.ClassFactory');
        }
        if (!function_exists('ReplaceFields')) {
            Yii::import('application.helpers.replacements_helper', true);
        }
        if (!class_exists('LimeExpressionManager', false)) {
            Yii::import('application.helpers.expressions.em_manager_helper', true);
        }
        if (!function_exists('gT')) {
            Yii::import('application.helpers.common_helper', true);
        }
        if (!function_exists('getLanguageRTL')) {
            Yii::import('application.helpers.surveytranslator_helper', true);
        }

        ClassFactory::registerClass('Token_', 'Token');
        ClassFactory::registerClass('Response_', 'Response');

        $defaulttheme = App()->getConfig('defaulttheme');
        /* Bad config set for rootdir */
        if (!is_dir(App()->getConfig('standardthemerootdir') . DIRECTORY_SEPARATOR . $defaulttheme) && !is_dir(App()->getConfig('userthemerootdir') . DIRECTORY_SEPARATOR . $defaulttheme)) {
            /* This included can be broken */
            $webroot = (string) Yii::getPathOfAlias('webroot');
            /* default config */
            $configFixed = array(
                'standardthemerootdir'   => $webroot . DIRECTORY_SEPARATOR . "themes" . DIRECTORY_SEPARATOR . "survey",
                'userthemerootdir'       => $webroot . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR . "themes" . DIRECTORY_SEPARATOR . "survey",
            );
            /* user config */
            $configConfig = require(Yii::getPathOfAlias('application') . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php');
            if (isset($configConfig['config'])) {
                $configFixed = array_merge($configFixed, $configConfig['config']);
            }
            $aConfigToFix = array(
                'standardthemerootdir',
                'userthemerootdir',
            );
            foreach ($aConfigToFix as $configToFix) {
                App()->setConfig($configToFix, $configFixed[$configToFix]);
            }
        }
        $test = \LimeExpressionManager::ProcessStepString("{TEST}", array(), 3, 1);
        $this->setRequestForCommand();
    }

    /**
     * return a valid SQL date
     * @parm string dateTime
     * @return string|null
     */
    private static function getValidDateTime($string)
    {
        $checked = DateTime::createFromFormat("Y-m-d H:i:s", $string);
        if ($checked) {
            return $checked->format("Y-m-d H:i:s");
            ;
        }
        $checked = DateTime::createFromFormat("Y-m-d H:i", $string);
        if ($checked) {
            return $checked->format("Y-m-d H:i:s");
            ;
        }
        $checked = DateTime::createFromFormat("Y-m-d H", $string);
        if ($checked) {
            return $checked->format("Y-m-d H:i:s");
            ;
        }
        $checked = DateTime::createFromFormat("!Y-m-d", $string);
        if ($checked) {
            return $checked->format("Y-m-d H:i:s");
            ;
        }
        return null;
    }

    /**
     * return the DB column
     * @param integer surveyId
     * @param string dateTime
     * @return string|false
     */
    private static function getFinalColumn($surveyId, $title)
    {
        $aQuestionCodeList = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
        return array_search($title, $aQuestionCodeList);
    }

    /**
     * Fix the current url
     */
    private function setRequestForCommand()
    {
        App()->request->hostInfo = $this->get("hostInfo");
        /* Issue with url manager and script … @todo : fix LimeSurvey core */
        $baseUrl = dirname(trim($this->get("baseUrl")));
        if (App()->getUrlManager()->showScriptName) {
            $baseUrl = $baseUrl . "/index.php";
        }
        App()->getUrlManager()->setBaseUrl($baseUrl);
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private function translate($string)
    {
        return Yii::t('', $string, array(), get_class($this) . 'Messages');
    }
}
