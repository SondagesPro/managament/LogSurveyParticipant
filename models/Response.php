<?php
/**
 * Log the difference between each action done
 * Extend CActiveRecord : no plugin event 
 */
namespace LogSurveyParticipant\models;
class Response extends \CActiveRecord
{
/**
 * Class LogSurveyParticipant\models\ReponseLog
 *
 * @property integer $id primary key
 * @property integer $sid : the survey
 * @property integer $srid : the response
 * @property integer $datestamp : the datetime
 * @property integer $state : The current state of response in json, only real response : no date, no step, no seed. Token and response column
*/

    /** @inheritdoc */
    public function init() {
        $this->datestamp = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig("timeadjust"));
    }

    /** @inheritdoc */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{logsurveyparticipant_response}}';
    }

    /** @inheritdoc */
    public function rules()
    {
        return array(
            array('id', 'numerical', 'integerOnly'=>true,'min'=>1),
            array('id', 'unique'),
            array('sid', 'numerical', 'integerOnly'=>true,'min'=>1),
            array('srid', 'numerical', 'integerOnly'=>true,'min'=>1),
            array('state', 'safe'),
        );
    }
}
