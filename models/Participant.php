<?php
/**
 * Log the difference between each action done
 * Extend CActiveRecord : no plugin event 
 */
namespace LogSurveyParticipant\models;
use CDBCriteria;
class Participant extends \CActiveRecord
{
    /**
     * Class LogSurveyParticipant\models\ParticipantResponseLog
     *
     * @property integer $id primary key
     * @property integer $sid : the survey
     * @property integer $srid : the response
     * @property string $token : the token
     * @property string $userdescription : the token information (1st name, last name)
     * @property string $useremail : the token information (email)
     * @property datetime $datestamp : the datetime
     * @property integer $count : count of update action
    */

    /** @inheritdoc */
    public function init() {
        $this->count = 1;
        $this->datestamp = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig("timeadjust"));
    }

    /** @inheritdoc */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{logsurveyparticipant_participant}}';
    }

    /** @inheritdoc */
    public function rules()
    {
        return array(
            array('id', 'numerical', 'integerOnly'=>true, 'min'=>1),
            array('id', 'unique'),
            array('sid', 'numerical', 'integerOnly'=>true, 'min'=>1),
            array('srid', 'numerical', 'integerOnly'=>true, 'min'=>1),
            array('token', 'length', 'min' => 0, 'max'=>50), // 35 in \Token
            array('userdescription', 'safe'),
            array('useremail', 'safe'),
            array('count', 'numerical', 'integerOnly'=>true, 'min'=>1),
        );
        return parent::rules();
    }

    /**
     * Add an update to user
     * Search last update done : if already by this user : update , else create a new one
     * @param $attributes the ary of the attriburtes
     * @return self::model() (can have error)
     */
    public static function addUpdate($attributes)
    {
        $surveyId = $attributes['sid'];
        $criteria = new CDBCriteria;
        $criteria->compare('sid', $attributes['sid']);
        $criteria->compare('srid', $attributes['srid']);
        $criteria->order = App()->db->quoteColumnName('datestamp').' DESC';
        $lastParticipantAction = self::model()->find($criteria);
        $sessionId = null;
        if (!empty($_SESSION["session_$surveyId"]['LogSurveyParticipantId'])) {
            /* Reset if it's a new sesssion … */
            $sessionId = $_SESSION["session_$surveyId"]['LogSurveyParticipantId'];
        }
        if (empty($lastParticipantAction) || $lastParticipantAction->token != $attributes['token'] || $lastParticipantAction->id != $sessionId) {
            $lastParticipantAction = new self;
            $lastParticipantAction->sid = $attributes['sid'];
            $lastParticipantAction->srid = $attributes['srid'];
            $lastParticipantAction->token = $attributes['token'];
            $lastParticipantAction->count = 0;
        }
        $lastParticipantAction->userdescription = $attributes['userdescription'];
        $lastParticipantAction->useremail = $attributes['useremail'];
        $lastParticipantAction->count++;
        $lastParticipantAction->datestamp = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig("timeadjust"));
        $lastParticipantAction->save();
        if($lastParticipantAction->id) {
            $_SESSION["session_$surveyId"]['LogSurveyParticipantId'] = $lastParticipantAction->id;
        }
        return $lastParticipantAction;
    }
}
