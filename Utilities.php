<?php
/**
 * Some Utilities
 * 
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 1.0.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
namespace LogSurveyParticipant;

use App;
use Yii;
class Utilities
{
    /* @var float give information for other plugin of Plugin API version */
    const API = 1.0;

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getSurveySetting($surveyId, $sSetting) {
        if(!Yii::getPathOfAlias('LogSurveyParticipant')) {
            return null;
        }

        $oPlugin = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'LogSurveyParticipant')
        );
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND '.App()->getDb()->quoteColumnName('key').' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => $oPlugin->id,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $surveyId,
            )
        );
        if(!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            return $value;
        }
        return null;
    }

}
